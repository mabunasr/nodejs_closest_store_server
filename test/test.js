/**
 * This file contains the normal flow and the basd flow test cases,
 * for the closest store api
 */
const assert = require('assert')
const request = require('supertest')
const express = require('express')
const storeRouter = require('../router/store.router')

const app = express()
app.use('/', storeRouter)

const TEST_STORE_ADDRESS = '1902 Miller Trunk Hwy'
const TEST_STORE_ZIP_CODE = '55811-1810'
const TEST_INVALID_STORE_ADDRESS = 'g768tftuv398yrhf7'
const TEST_INVALID_STORE_ZIP_CODE = '9876b75gjuy'
var storeDistanceInMiles = undefined
var storeDistanceInKilometers = undefined

describe('Closest store api normal flow unit test', () => {
    /**
     * The normal flow test cases for the closest store api
     */

    describe('GET /closest?address=' + encodeURI(TEST_STORE_ADDRESS), () => {
        /**
         * Get the closest store to the provided address
         */
        it('get the closest store to a provided address', (done) => {
            request(app)
                .get('/closest?address=' + encodeURI(TEST_STORE_ADDRESS))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, true)
                    assert.equal(response.body.message, "Nearest store has been retrieved successfully")
                    assert.equal(decodeURI(response.body.data.store.address), TEST_STORE_ADDRESS)
                    done()
                })
        })
    })

    describe('GET /closest?zip=' + encodeURI(TEST_STORE_ZIP_CODE), () => {
        /**
         * Get the closest store to the provided zip code
         */
        it('get the closest store to a provided zip code', (done) => {
            request(app)
                .get('/closest?zip=' + encodeURI(TEST_STORE_ZIP_CODE))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, true)
                    assert.equal(response.body.message, "Nearest store has been retrieved successfully")
                    assert.equal(decodeURI(response.body.data.store.address), TEST_STORE_ADDRESS)
                    storeDistanceInMiles = response.body.data.store.distance
                    done()
                })
        })
    })

    describe('GET /closest?units=km&address=' + encodeURI(TEST_STORE_ADDRESS), () => {
        /**
         * Get the closest store to the provided address with distance in kilometers
         */
        it('get the closest store to a provided address and units=km', (done) => {
            request(app)
                .get('/closest?units=km&zip=' + encodeURI(TEST_STORE_ADDRESS))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, true)
                    assert.equal(response.body.message, "Nearest store has been retrieved successfully")
                    assert.equal(decodeURI(response.body.data.store.address), TEST_STORE_ADDRESS)
                    storeDistanceInKilometers = response.body.data.store.distance
                    assert.equal(storeDistanceInKilometers > storeDistanceInMiles, true)
                    done()
                })
        })
    })

    describe('GET /closest?units=mi&address=' + encodeURI(TEST_STORE_ADDRESS), () => {
        /**
         * Get the closest store to the provided address with distance in miles
         */
        it('get the closest store to a provided address and units=mi', (done) => {
            request(app)
                .get('/closest?units=mi&address=' + encodeURI(TEST_STORE_ADDRESS))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, true)
                    assert.equal(response.body.message, "Nearest store has been retrieved successfully")
                    assert.equal(decodeURI(response.body.data.store.address), TEST_STORE_ADDRESS)
                    done()
                })
        })
    })

    describe('GET /closest?units=km&zip=' + encodeURI(TEST_STORE_ZIP_CODE), () => {
        /**
         * Get the closest store to the provided zip code with distance in kilometers
         */
        it('get the closest store to a provided zip code and units=km', (done) => {
            request(app)
                .get('/closest?units=km&zip=' + encodeURI(TEST_STORE_ZIP_CODE))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, true)
                    assert.equal(response.body.message, "Nearest store has been retrieved successfully")
                    assert.equal(decodeURI(response.body.data.store.address), TEST_STORE_ADDRESS)
                    storeDistanceInKilometers = response.body.data.store.distance
                    assert.equal(storeDistanceInKilometers > storeDistanceInMiles, true)
                    done()
                })
        })
    })

    describe('GET /closest?units=mi&zip=' + encodeURI(TEST_STORE_ZIP_CODE), () => {
        /**
         * Get the closest store to the provided zip code with distance in miles
         */
        it('get the closest store to a provided zip code and units=mi', (done) => {
            request(app)
                .get('/closest?units=mi&zip=' + encodeURI(TEST_STORE_ZIP_CODE))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, true)
                    assert.equal(response.body.message, "Nearest store has been retrieved successfully")
                    assert.equal(decodeURI(response.body.data.store.address), TEST_STORE_ADDRESS)
                    done()
                })
        })
    })

})

describe('Closest store api bad flow unit test', () => {
    /**
     * The bad flow test cases for the closest store api
     */
    describe('GET /invalid', () => {
        /**
         * Send request to an invalid api route
         */
        it('send request to an invalid api route', (done) => {
            request(app)
                .get('/invalid')
                .expect(404)
                done()
        })
    })

    describe('GET /closest', () => {
        /**
         * Send request to the api without any query string parameter
         */
        it('send request to the api without any query string parameter', (done) => {
            request(app)
                .get('/closest')
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, false)
                    assert.equal(response.body.message, "Error: Address or Zip code must be provided")
                    done()
                })
        })
    })

    describe('GET /closest?address=' + encodeURI(TEST_INVALID_STORE_ADDRESS), () => {
        /**
         * Get the closest store to a provided invalid address
         */
        it('get the closest store to a provided invalid address', (done) => {
            request(app)
                .get('/closest?address=' + encodeURI(TEST_INVALID_STORE_ADDRESS))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, false)
                    assert.equal(response.body.message, "Error: failed to get the location geodata")
                    done()
                })
        })
    })

    describe('GET /closest?zip=' + encodeURI(TEST_INVALID_STORE_ZIP_CODE), () => {
        /**
         * Get the closest store to a provided invalid zip code
         */
        it('get the closest store to a provided invalid zip code', (done) => {
            request(app)
                .get('/closest?zip=' + encodeURI(TEST_INVALID_STORE_ZIP_CODE))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, false)
                    assert.equal(response.body.message, "Error: failed to get the location geodata")
                    done()
                })
        })
    })

    describe('GET /closest?units=invalid&address=' + encodeURI(TEST_STORE_ADDRESS), () => {
        /**
         * Get the closest store to a provided address and invalid distance unit
         */
        it('get the closest store to a provided address and units=invalid', (done) => {
            request(app)
                .get('/closest?units=invalid&address=' + encodeURI(TEST_STORE_ADDRESS))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, false)
                    assert.equal(response.body.message, "Error: Invalid units value, allowed values: (mi|km)")
                    done()
                })
        })
    })

    describe('GET /closest?units=invalid&zip=' + encodeURI(TEST_STORE_ZIP_CODE), () => {
        /**
         * Get the closest store to a provided zip code and invalid distance unit
         */
        it('get the closest store to a provided zip code and units=invalid', (done) => {
            request(app)
                .get('/closest?units=invalid&zip=' + encodeURI(TEST_STORE_ZIP_CODE))
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, false)
                    assert.equal(response.body.message, "Error: Invalid units value, allowed values: (mi|km)")
                    done()
                })
        })
    })

    describe('GET /closest?address=', () => {
        /**
         * Get the closest store to a provided empty address value
         */
        it('get the closest store to a provided empty address value', (done) => {
            request(app)
                .get('/closest?address=')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, false)
                    assert.equal(response.body.message, "Error: failed to get the location geodata")
                    done()
                })
        })
    })

    describe('GET /closest?zip=', () => {
        /**
         * Get the closest store to a provided empty zip code value
         */
        it('get the closest store to a provided empty zip code value', (done) => {
            request(app)
                .get('/closest?zip=')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((error, response) => {
                    if (error)
                        throw error
                    assert.equal(response.body.status, false)
                    assert.equal(response.body.message, "Error: failed to get the location geodata")
                    done()
                })
        })
    })
})