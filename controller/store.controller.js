/*
The store controller module contains the required functions to process the client requests
and return the result after it finish processing.
 */
const path = require("path") //the path module, used to resolve the stores data csv file path
const csv = require('csvtojson') //the csvtojson module, used to read the stores data file and convert it to a json object
const NodeGeocoder = require('node-geocoder') //the node-geocoder module, used to get the coordinates of the received address or zip code
const NodeCache = require("node-cache") //the node-cache module, used to catch the data
const appConfig = require('../config/app.config') //the app config module, contains the required configuration constants
const AppUtil = require('../util/appUtil') //the app util module, contains a helper functions for this application

const DISTANCE_UNIT_MI = 'mi'
const DISTANCE_UNIT_KM = 'km'
const DISTANCE_MI_TO_KM = 1.609
const NODE_CACHE_STORES_DATA_KEY = "stores_data"

/** 
 *create a NodeCache instance, will be used to catch the stores data csv file, 
 *to prevent the controller from reading the file on each request.
 * */
const nodeCache = new NodeCache()
//configure the gecoding module
const geoCoder = NodeGeocoder({
    provider: appConfig.GEOCODER_SERVICE_PROVIDER,
    apiKey: appConfig.GEOCODER_API_KEY
})
const util = AppUtil()

var storeController = {}

storeController.getClosestStore = async (data) => {
    /*
    This function used to process the client request to the closest store api route.
    args:
        data: object contains the request query string parameters
    returns:
        result: object contains the operation status, response message and response data if provided
     */

    const address = decodeURI(data.address)
    const zip = decodeURI(data.zip)
    const units = decodeURI(data.units)

    var result = {
        status: false, //the operation status
        message: '', //the response message
        data: '' //the response data if provided
    }

    try {
        //validate the request query string parameters
        var location = undefined
        if (address != 'undefined') {
            location = address
        }
        else if (zip != 'undefined') {
            location = zip
        }
        else {
            throw "Error: Address or Zip code must be provided"
        }
        if (units != 'undefined' && units != DISTANCE_UNIT_KM && units != DISTANCE_UNIT_MI) {
            throw "Error: Invalid units value, allowed values: (mi|km)"
        }
        //read the stores data file and validate it's not empty
        var storesData = nodeCache.get(NODE_CACHE_STORES_DATA_KEY)
        if (storesData == undefined) {
            storesData = await csv().fromFile(path.resolve(appConfig.DATA_FILE_PATH))
            nodeCache.set(NODE_CACHE_STORES_DATA_KEY, storesData)
        }
        if (storesData.length == 0)
            throw "Error: store locations data is empty"

        //get the received location geocode data and validate it's not empty
        const geoData = await geoCoder.geocode(location)
        if (geoData.length == 0)
            throw "Error: failed to get the location geodata"
        //extract the location coordinates from the data 
        const lat = geoData[0].latitude
        const lon = geoData[0].longitude
        //calculate the distance (as the crow flies) between the received location and each store and get the closest store
        var nearestStoreIndex = 0
        var nearestStore = util.getDistance(lat, lon, storesData[nearestStoreIndex].Latitude, storesData[nearestStoreIndex].Longitude)
        for (var i = 1; i < storesData.length; i++) {
            var distance = util.getDistance(lat, lon, storesData[i].Latitude, storesData[i].Longitude)
            if (nearestStore > distance) {
                nearestStore = distance
                nearestStoreIndex = i
            }
        }
        //convert the distance from kilometers to miles if needed
        if (units != DISTANCE_UNIT_KM)
            nearestStore = nearestStore / DISTANCE_MI_TO_KM
        //set the operation status to success
        result.status = true
        //set the request result data
        result.data = { store: { address: encodeURI(storesData[nearestStoreIndex].Address), distance: nearestStore } }
        //set the request message
        result.message = "Nearest store has been retrieved successfully"
    } catch (ex) {
        //set the operation status to failed if any error occur
        result.status = false
        //set the response error message
        result.message = ex
    }
    return result //return the response result
}

module.exports = storeController