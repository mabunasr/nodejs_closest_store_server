Find the closest store web api.  
  
Overview:  
  
It's a simple node-js server which can fetch the nearest store address and distance to a provided
address or zip code, it works as follows:  
1- the server receives the request  
2- invoke the required router handler function or middleware  
3- the router handler function passes the query string data to the store controller  
4- the store controller extracts and validates the provided query string data  
5- the store controller loads the store locations data from the csv file  
6- the store controller gets the geocode data (latitude, longitude) for the provided address or zip code  
7- the store controller calculates the distance between the provided address or zip code and each store location on the store locations list  
8- the store controller converts the distance unit from "km" to the required unit  
9- the store controller prepare an object that contains the operation status, message and the result data and returns it to the router handler function  
  
  
Used node-js modules:  
  
1- express - used for creating the server  
2- path - used for resolving the stores data file path  
3- csvtojson - used for reading the stores csv data  
4- node-geocoder - used for geocoding the provided address or zip code  
5- assert - used for comparing values in the test  
6- supertest - user for making requests to the server in the test  
7- mocha - the test framework  
  
  
How to run:  
  
1- open the cmd and enter the project directory  
2- run: npm install  
3- run: npm start  
  
  
How to run the unit test:  
  
1- open the cmd and enter the project directory  
2- run: npm test  
  
  
How to use:  
  
- After running the server, it will listen to the port 80, "you can modify the port in the /config/app.config.js"  
- Use the browser or any REST Api client like Postman and make get requests to the following link:  
"localhost/closest?address=address"  
"localhost/closest?address=address&units=mi|km"  
"localhost/closest?zip=zip"  
"localhost/closest?zip=zip&units=mi|km"  
