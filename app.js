/**
 * Node-js server used to get the closest store address to a givin address or zip code
 */
const express = require('express')// the express module, used to create the server
const appConfig = require('./config/app.config') //the app config module, contains the required configuration constants
const storeRouter = require('./router/store.router') //the store router module, contains the required routes for the store api

//create the express server instance
const app = express()
//add the store api middlewares
app.use('/', storeRouter)
//start listining on the server port specified in the app config
app.listen(appConfig.SERVER_PORT, () => {
    console.log('Server is running on port: ' + appConfig.SERVER_PORT)
})