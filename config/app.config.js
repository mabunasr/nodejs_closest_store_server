/*
This module contains the applications's configuration constants
 */
const appConfig = {
    //the server port
    SERVER_PORT: 80,
    //the stores data csv file path
    DATA_FILE_PATH: "./data/store-locations.csv",
    //the geocoding service provider, will be used by the node-geocoder library
    GEOCODER_SERVICE_PROVIDER: "opencage",
    //the geocoding service provider api key, will be used by the node-geocoder library
    GEOCODER_API_KEY: "2db6d1ccf20a439e93041c864a980366"
}

module.exports = appConfig