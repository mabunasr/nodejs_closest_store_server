/*
This module contains the required routs for the closest store api
 */
const express = require('express')
const storeController = require('../controller/store.controller')

const storeRouter = express.Router()

//this middleware handles the client GET requests to the closest store api
storeRouter.get('/closest', async (req, res, next) => {
    //process the client request using the store controller
    var result = await storeController.getClosestStore(req.query)
    res.json(result)
})

module.exports = storeRouter