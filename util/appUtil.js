/**
 * This module contains a helper functions for the application
 */
module.exports = function () {
    this.getDistance = (lat1, lon1, lat2, lon2) => {
        /*
        This function used to calculate the distance between two points on earth
        params:
            lat1: the first point latitude value
            lon1: the first point longitude value
            lat2: the second point latitude value
            lon2: the second point longitude value

        returns:
            distance: the distance between the two points
         */

        const R = 6371
        const dLat = getRadius(lat2 - lat1)
        const dLon = getRadius(lon2 - lon1)
        const a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(getRadius(lat1)) * Math.cos(getRadius(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)

        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        const d = R * c
        return d
    }

    function getRadius(deg) {
        return deg * (Math.PI / 180)
    }

    return this
}